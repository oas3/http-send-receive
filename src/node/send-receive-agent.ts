import { AbortError } from "abort-tools";
import * as http from "http";
import { Writable } from "stream";
import { hasHeaderValue, HttpSendReceiveHeaders } from "../headers.js";
import { HttpReceiveResponse } from "../receive-response.js";
import { HttpSendReceive } from "../send-receive.js";
import { HttpSendRequest } from "../send-request.js";
import { ConnectTimeoutError, IdleTimeoutError, waitForError, waitForEvent, withAbort } from "../utils/index.js";

export interface HttpAgentSendReceiveOptions {
    agent?: http.Agent,
    connectTimeout?: number,
    idleTimeout?: number,
}

export function createHttpAgentSendReceive(
    options: HttpAgentSendReceiveOptions = {},
): HttpSendReceive {
    return async (
        sendRequest: HttpSendRequest,
    ) => {
        const expect100 = hasHeaderValue(sendRequest.headers, "expect", "100-continue");

        const request = http.request(sendRequest.url, {
            agent: options.agent,
            method: sendRequest.method,
            headers: sendRequest.headers,
            timeout: options.connectTimeout,
        });
        request.setTimeout(options.idleTimeout ?? 0);
        request.addListener("timeout", () => {
            if (request.socket == null || request.socket.connecting) {
                request.destroy(new ConnectTimeoutError(sendRequest.url));
            }
            else {
                request.destroy(new IdleTimeoutError(sendRequest.url));
            }
        });

        if (sendRequest.body == null) {
            request.end();
        }

        const response = await withAbort(async signal => {
            const errorPromise = waitForError(signal, request);
            const responsePromise = waitForEvent<http.IncomingMessage>(signal, request, "response");

            if (expect100) {
                const continuePromise = waitForEvent<void>(signal, request, "continue");

                const response = await Promise.race([
                    errorPromise,
                    continuePromise,
                    responsePromise,
                ]);
                if (response) return response;
            }

            if (sendRequest.body != null) {
                const response = await Promise.race([
                    errorPromise,
                    responsePromise,
                    writeBody(sendRequest.body(signal), request),
                ]);
                if (response) return response;
            }

            const response = await Promise.race([
                errorPromise,
                responsePromise,
            ]);
            return response;
        });

        request.addListener(
            "error",
            error => response.destroy(error),
        );

        const receiveResponse: HttpReceiveResponse = {
            status: response.statusCode ?? 0,
            /*
            we need the cast here, because res.headers is of type IncomingHttpHeaders
            which extends NodeJS.Dict, which has values that can be undefined!
            */
            headers: response.headers as HttpSendReceiveHeaders,
            /**/
            body(signal) {
                // this will also destroy response
                const abort = () => request.destroy(new AbortError("http response aborted"));
                if (signal?.aborted) {
                    abort();
                }
                else {
                    signal?.addEventListener("abort", abort);
                    response.addListener(
                        "close",
                        () => signal?.removeEventListener("abort", abort),
                    );
                }
                return response;
            },
        };

        return receiveResponse;
    };

}

async function writeBody(
    body: AsyncIterable<Uint8Array>,
    req: Writable,
) {
    for await (const chunk of body) {
        await new Promise<void>((resolve, reject) => {
            req.write(chunk, error => error ? reject(error) : resolve());
        });
    }

    await new Promise<void>(resolve => {
        req.end(() => resolve());
    });
}
