import { HttpSendReceiveBodyFactory } from "./body.js";
import { HttpSendReceiveHeaders } from "./headers.js";

export interface HttpReceiveResponse {
    status: number;
    headers: HttpSendReceiveHeaders;
    body: HttpSendReceiveBodyFactory;
}
