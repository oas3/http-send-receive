import { HttpReceiveResponse } from "./receive-response.js";
import { HttpSendRequest } from "./send-request.js";

export interface HttpSendReceive {
    (sendRequest: HttpSendRequest): Promise<HttpReceiveResponse>;
}
