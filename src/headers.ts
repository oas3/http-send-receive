export type HttpSendReceiveHeaders = Record<string, string | string[]>;

export function hasHeaderValue(
    headers: HttpSendReceiveHeaders,
    headerName: string,
    expectValue: string,
) {
    const headerValue = headers[headerName];

    if (typeof headerValue === "string") {
        return headerValue === expectValue;
    }

    if (Array.isArray(headerValue)) {
        for (const headerValueItem of headerValue) {
            if (headerValueItem === expectValue) {
                return true;
            }
        }
    }

    return false;
}
