import assert from "assert";
import delay from "delay";
import * as fs from "fs";
import * as http from "http";
import * as http2 from "http2";
import * as https from "https";
import { second } from "msecs";
import { Readable, Writable } from "stream";

const type = process.argv[2];

let baseUrl: URL;
switch (type) {
    case "http":
    case "h2c":
        baseUrl = new URL("http://localhost:8080/");
        break;

    case "https":
    case "h2":
        baseUrl = new URL("https://localhost:8080/");
        break;

    default: assert.fail();
}

const handler = async (
    req: (http.IncomingMessage | http2.Http2ServerRequest) & Readable,
    res: (http.ServerResponse | http2.Http2ServerResponse) & Writable,
) => {
    const origin = req.headers["origin"];
    if (origin != null) {
        res.setHeader("access-control-allow-origin", origin);
    }

    const decoder = new TextDecoder();
    const encoder = new TextEncoder();

    let body = "";

    try {
        for await (const chunk of req) {
            body += decoder.decode(chunk, { stream: true });
        }

        for (const line of body.split("\n")) {
            const chunk = encoder.encode(line + "\n");
            await new Promise<void>(
                (resolve, reject) => res.write(
                    chunk,
                    error => error == null ? resolve() : reject(error),
                ),
            );
            await delay(1 * second);
        }
        await new Promise<void>(
            resolve => res.end(
                () => resolve(),
            ),
        );
    }
    catch (error) {
        if (!(error instanceof Error)) throw error;
        if (!("code" in error)) throw error;
        if (!(error.code === "ERR_STREAM_DESTROYED")) throw error;
    }

};

let server:
    | http.Server
    | https.Server
    | http2.Http2Server
    | http2.Http2SecureServer;

switch (type) {
    case "http":
        server = http.createServer(
            {},
            handler,
        );
        break;

    case "https":
        server = https.createServer(
            {
                ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                key: fs.readFileSync("./tls/key.pem", "utf8"),
            },
            handler,
        );
        break;

    case "h2c":
        server = http2.createServer(
            {
            },
            handler,
        );
        break;

    case "h2":
        server = http2.createSecureServer(
            {
                ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                key: fs.readFileSync("./tls/key.pem", "utf8"),
            },
            handler,
        );
        break;

    default: assert.fail();
}

server.listen(baseUrl.port, () => {
    process.send?.([
        "listening",
        baseUrl.toString(),
    ]);
});
