export async function withAbort<T>(
    job: (signal: AbortSignal) => T | Promise<T>,
): Promise<T> {
    const abortController = new AbortController();
    try {
        const result = await job(abortController.signal);
        return result;
    }
    finally {
        abortController.abort();
    }
}

