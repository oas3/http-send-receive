export class ConnectTimeoutError extends Error {
    readonly name = "ConnectTimeoutError";

    constructor(public url: URL) {
        super(`connect timeout for ${url}`);
    }
}

export class IdleTimeoutError extends Error {
    readonly name = "IdleTimeoutError";

    constructor(public url: URL) {
        super(`idle timeout for ${url.href}`);
    }
}

export class PingFailedError extends Error {
    readonly name = "PingFailedError";

    constructor(public url: URL) {
        super(`ping failed for ${url}`);
    }
}
